package internship;

import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

import static java.util.Objects.isNull;

public class StudentSet implements Set<Student> {
    private static final Object PRESENT = new Object();
    private HashMap<Student, Object> map;

    public StudentSet() {
        map = new HashMap<>();
    }

    public StudentSet(Collection<Student> studentCollection) {
        map = new HashMap<>(Math.max((int) (studentCollection.size() / .75f) + 1, 16));
        addAll(studentCollection);
    }

    @Override
    public int size() {
        return map.size();
    }

    @Override
    public boolean isEmpty() {
        return map.isEmpty();
    }

    @Override
    public boolean contains(Object o) {
        if (isNull(o))
            return false;
        return map.containsKey(o);
    }

    @Override
    public Iterator<Student> iterator() {
        return map.keySet().iterator();
    }

    @Override
    public Student[] toArray() {
        return map.keySet().toArray(new Student[0]);
    }

    @Override
    public <T> T[] toArray(T[] ts) {
        return (T[]) map.keySet().toArray(new Student[0]);
    }

    @Override
    public boolean add(Student student) {
        return map.put(student, PRESENT) == null;
    }

    @Override
    public boolean remove(Object o) {
        return map.remove(o) == PRESENT;
    }

    @Override
    public boolean containsAll(Collection<?> collection) {
        return map.keySet().containsAll(collection);
    }

    @Override
    public boolean addAll(Collection<? extends Student> collection) {
        boolean added = false;
        for (Student student : collection)
            if (add(student))
                added = true;
        return added;
    }

    @Override
    public boolean retainAll(Collection<?> collection) {
        return map.keySet().retainAll(collection);
    }

    @Override
    public boolean removeAll(Collection<?> collection) {
        return map.keySet().removeAll(collection);
    }

    @Override
    public void clear() {
        map.clear();
    }
}
