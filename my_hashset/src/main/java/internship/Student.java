package internship;

import java.time.LocalDate;

import static java.util.Objects.isNull;

public class Student implements Comparable<Student> {
    private final String name;
    private final LocalDate dateOfBirth;
    private final String details;

    public Student(String name, LocalDate dateOfBirth, String details) {
        this.name = name;
        this.dateOfBirth = dateOfBirth;
        this.details = details;
    }

    public String getName() {
        return name;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public String getDetails() {
        return details;
    }

    @Override
    public int compareTo(Student s) {
        if (name.equals(s.name)) {
            return dateOfBirth.compareTo(s.dateOfBirth);
        }

        return name.compareTo(s.name);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (isNull(o) || o.getClass() != this.getClass()) {
            return false;
        }

        Student student = (Student) o;
        return name != null
                && name.equals(student.getName())
                && dateOfBirth != null
                && dateOfBirth.equals(student.getDateOfBirth());
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int hash = 1;
        hash = prime * hash + ((name == null) ? 0 : name.hashCode());
        hash = prime * hash + ((dateOfBirth == null) ? 0 : dateOfBirth.hashCode());

        return hash;
    }
}
