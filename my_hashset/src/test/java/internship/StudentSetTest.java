package internship;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.assertThat;


class StudentSetTest {
    private final StudentSet set = new StudentSet();

    private static final Student STUDENT1 = new Student("Anna", LocalDate.of(1999, 5, 26), "details1");
    private static final Student STUDENT2 = new Student("Max", LocalDate.of(2002, 11, 12), "details2");
    private static final Student STUDENT3 = new Student("Alex", LocalDate.of(2000, 12, 15), "details3");

    @BeforeEach
    void setup() {
        set.add(STUDENT1);
        set.add(STUDENT2);
        set.add(STUDENT3);
    }

    @AfterEach
    void cleanup() {
        set.clear();
    }

    @Test
    void shouldNotBeAbleToAddIdenticalObjects() {
        assertThat(set.add(STUDENT1)).isTrue();
        assertThat(set.size()).isEqualTo(3);
        assertThat(set).containsExactlyInAnyOrder(STUDENT1, STUDENT2, STUDENT3);
    }

    @Test
    void shouldCreateNewSetAddingAllStudentsFromAnotherSet() {
        StudentSet studentSet2 = new StudentSet(set);
        assertThat(studentSet2).containsExactlyInAnyOrder(STUDENT1, STUDENT2, STUDENT3);
    }

    @Test
    void shouldBeThreeStudentsInTheSetAfterSetup() {
        assertThat(set.size()).isEqualTo(3);
        assertThat(set).containsExactlyInAnyOrder(STUDENT1, STUDENT2, STUDENT3);
    }

    @Test
    void shouldReturnFalseWhenSetDoesNotContainsThisStudent() {
        final Student studentNotExist = new Student("Kate", LocalDate.of(1998, 3, 2), "details4");

        assertThat(set.contains(studentNotExist)).isFalse();
    }

    @Test
    void shouldReturnFalseWhenStudentIsNull() {
        assertThat(set.contains(null)).isFalse();
    }

    @Test
    void shouldRemoveStudentFromSet() {
        assertThat(set.remove(STUDENT1)).isTrue();
        assertThat(set.contains(STUDENT1)).isFalse();
    }

    @Test
    void shouldReturnFalseWhenRemoveStudentNotExist() {
        final Student studentNotExist = new Student("Kate", LocalDate.of(1998, 3, 2), "details4");

        assertThat(set.remove(studentNotExist)).isFalse();
    }

    @Test
    void shouldFindAllStudents_ReturnTrue() {
        final StudentSet set2 = new StudentSet();
        set2.add(STUDENT1);
        set2.add(STUDENT2);

        assertThat(set.containsAll(set2)).isTrue();
    }

    @Test
    void shouldNotFindAllStudents_ReturnFalse() {
        final Student studentNotExist = new Student("Kate", LocalDate.of(1998, 3, 2), "details4");
        final StudentSet set2 = new StudentSet();

        set2.add(STUDENT1);
        set2.add(STUDENT2);
        set2.add(studentNotExist);

        assertThat(set.containsAll(set2)).isFalse();
    }

    @Test
    void shouldRemoveAllStudentsFromSet() {
        final StudentSet set2 = new StudentSet();
        set2.add(STUDENT1);
        set2.add(STUDENT2);

        assertThat(set.removeAll(set2)).isTrue();
        assertThat(set.size()).isEqualTo(1);
        assertThat(set).doesNotContain(STUDENT1, STUDENT2);
    }

    @Test
    void shouldRetainAllStudentsFromSet() {
        final StudentSet set2 = new StudentSet();
        set2.add(STUDENT1);
        set2.add(STUDENT2);

        assertThat(set.retainAll(set2)).isTrue();

        assertThat(set.size()).isEqualTo(2);
        assertThat(set).containsExactlyInAnyOrder(STUDENT1, STUDENT2);
        assertThat(set).doesNotContain(STUDENT3);
    }

    @Test
    void shouldReturnArrayOfStudents() {
        Student[] studentArray = set.toArray();
        assertThat(studentArray).containsExactlyInAnyOrder(STUDENT1, STUDENT2, STUDENT3);
    }

    @Test
    void shouldDeleteAllStudentsFromTheSet() {
        set.clear();
        assertThat(set.isEmpty()).isTrue();
    }

}
