package internship;

import org.junit.jupiter.api.Test;

import java.time.LocalDate;

import static org.assertj.core.api.Assertions.catchThrowable;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

public class StudentTest {

    private final static Student STUDENT1 = new Student("Anna", LocalDate.of(1999, 5, 26), "details1");
    private final static Student STUDENT2 = new Student("Anna", LocalDate.of(1999, 5, 26), "details2");

    @Test
    void reflexivityTest() {
        assertThat(STUDENT1).isEqualTo(STUDENT1);
    }

    @Test
    void symmetricityTest() {
        assertThat(STUDENT2.equals(STUDENT1)).isEqualTo(STUDENT1.equals(STUDENT2));
    }

    @Test
    void transitivityTest() {
        Student student3 = new Student("Anna", LocalDate.of(1999, 5, 26), "details3");

        assertThat(STUDENT1).isEqualTo(STUDENT2);
        assertThat(STUDENT2).isEqualTo(student3);
        assertThat(STUDENT1).isEqualTo(student3);
    }

    @Test
    void consistencyTest() {
        Student student2 = new Student("Anna", LocalDate.of(1999, 5, 26), "details1");

        assertThat(STUDENT1).isEqualTo(student2);
        assertThat(STUDENT1).isEqualTo(student2);

        student2 = new Student("Max", LocalDate.of(1998, 7, 26), "details4");

        assertThat(STUDENT1).isNotEqualTo(student2);
    }

    @Test
    void shouldReturnFalseWhenEquatesToNull() {
        assertThat(STUDENT1).isNotNull();
        assertThat(STUDENT1).isNotEqualTo(null);
    }

    @Test
    void shouldReturnFalseWhenEquatesToNonStudent() {
        Integer integer = 5;
        assertThat(STUDENT1).isNotEqualTo(integer);
    }

    @Test
    void shouldReturnFalseWhenEquatesToDifferentValue() {
        final Student different_student = new Student("Max", LocalDate.of(1998, 7, 26), "details4");
        assertThat(STUDENT1).isNotEqualTo(different_student);
    }

    @Test
    void shouldBeEqualHashcodeWhenObjectsEqual() {
        assertThat(STUDENT1).isEqualTo(STUDENT2);
        assertThat(STUDENT1.hashCode()).isEqualTo(STUDENT2.hashCode());
    }

    @Test
    void shouldNotBeEqualWhenHashcodeNotEqual() {
        final Student different_student = new Student("Max", LocalDate.of(1998, 7, 26), "details4");
        if (STUDENT1.hashCode() != different_student.hashCode())
            assertThat(STUDENT1).isNotEqualTo(different_student);
    }

    @Test
    void shouldReturnThatStudent1LessThanStudent2() {
        final Student Student2 = new Student("Max", LocalDate.of(1998, 7, 26), "details4");

        assertThat(STUDENT1.compareTo(Student2)).isLessThan(0);
    }

    @Test
    void shouldReturnThatStudent2LessThanStudent1() {
        final Student Student2 = new Student("Anna", LocalDate.of(2000, 6, 27), "details1");

        assertThat(STUDENT1.compareTo(Student2)).isLessThan(0);
    }

    @Test
    void shouldReturnZeroWhenStudentsAreEqual() {
        final Student copyStudent1 = new Student("Anna", LocalDate.of(1999, 5, 26), "details1");
        assertThat(STUDENT1.compareTo(copyStudent1)).isEqualTo(0);
    }

    @Test
    void shouldThrowNullPointerException() {
        Throwable thrown = catchThrowable(() -> {
            STUDENT1.compareTo(null);
        });
        assertThat(thrown).isInstanceOf(NullPointerException.class);
    }
}
