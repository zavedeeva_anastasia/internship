package com.endava.internship.mocking.service;

import com.endava.internship.mocking.model.Payment;
import com.endava.internship.mocking.model.Status;
import com.endava.internship.mocking.model.User;
import com.endava.internship.mocking.repository.PaymentRepository;
import com.endava.internship.mocking.repository.UserRepository;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PaymentServiceTest {
    @Mock
    private UserRepository userRepository;
    @Mock
    private PaymentRepository paymentRepository;
    @Mock
    private ValidationService validationService;

    private PaymentService paymentService;

    private final ArgumentCaptor<Integer> userIdCaptor = ArgumentCaptor.forClass(Integer.class);

    private final ArgumentCaptor<Double> amountCaptor = ArgumentCaptor.forClass(Double.class);

    private final ArgumentCaptor<User> userCaptor = ArgumentCaptor.forClass(User.class);

    private final ArgumentCaptor<String> messageCaptor = ArgumentCaptor.forClass(String.class);

    private final ArgumentCaptor<UUID> paymentIdCaptor = ArgumentCaptor.forClass(UUID.class);

    @BeforeEach
    void setUp() {
        paymentService = new PaymentService(userRepository, paymentRepository, validationService);
    }

    @Test
    void createPayment() {
        final User testUser = new User(1, "Maria", Status.ACTIVE);

        final Integer testPaymentUserId = 1;
        final Double testPaymentAmount = 200.0;
        final Payment testPayment = new Payment(testPaymentUserId, testPaymentAmount, "Payment from user Maria");

        when(userRepository.findById(1)).thenReturn(Optional.of(testUser));
        when(paymentRepository.save(any())).thenReturn(testPayment);

        final Payment resultPayment = paymentService.createPayment(1, 200.0);
        assertThat(resultPayment).isEqualTo(testPayment);

        verify(validationService).validateUserId(userIdCaptor.capture());
        verify(validationService).validateAmount(amountCaptor.capture());
        verify(validationService).validateUser(userCaptor.capture());

        assertThat(userIdCaptor.getValue()).isEqualTo(testPaymentUserId);
        assertThat(amountCaptor.getValue()).isEqualTo(testPaymentAmount);
        assertThat(userCaptor.getValue()).isEqualTo(testUser);
    }

    @Test
    void createPaymentShouldThrowException(){
        Throwable thrown = catchThrowable(() -> paymentService.createPayment(1, 200.0));
        assertThat(thrown).isInstanceOf(NoSuchElementException.class);

        verify(validationService).validateUserId(userIdCaptor.capture());
        verify(validationService).validateAmount(amountCaptor.capture());
    }

    @Test
    void editMessage() {
        final Integer testPaymentUserId = 1;
        final Double testPaymentAmount = 200.0;
        final String testPaymentMessage = "newMessage";
        final Payment testPayment = new Payment(testPaymentUserId, testPaymentAmount, testPaymentMessage);

        final UUID testPaymentId = testPayment.getPaymentId();

        when(paymentRepository.editMessage(any(), any())).thenReturn(testPayment);

        final Payment result = paymentService
            .editPaymentMessage(testPaymentId, testPaymentMessage);

        assertThat(result).isEqualTo(testPayment);

        verify(validationService).validateMessage(messageCaptor.capture());
        verify(validationService).validatePaymentId(paymentIdCaptor.capture());

        assertThat(messageCaptor.getValue()).isEqualTo(testPaymentMessage);
        assertThat(paymentIdCaptor.getValue()).isEqualTo(testPaymentId);
    }

    @Test
    void getAllByAmountExceeding() {
        final Payment payment1 = new Payment(1, 200.0, "mes1");
        final Payment payment2 = new Payment(2, 250.0, "mes2");
        final Payment payment3 = new Payment(1, 300.0, "mes3");

        final List<Payment> payments = new ArrayList<>();
        payments.add(payment1);
        payments.add(payment2);
        payments.add(payment3);

        final List<Payment> paymentsExpected = new ArrayList<>();
        paymentsExpected.add(payment2);
        paymentsExpected.add(payment3);

        when(paymentRepository.findAll()).thenReturn(payments);

        List<Payment> resultList = paymentService.getAllByAmountExceeding(220.0);
        assertThat(resultList).isEqualTo(paymentsExpected);
    }
}
