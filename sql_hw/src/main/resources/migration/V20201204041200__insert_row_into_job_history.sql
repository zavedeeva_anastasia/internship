-- for inserting into job_histroy, I must fill their parent tables
-- and parent tables for parent tables


-- insert into regions
INSERT INTO regions (region_id, region_name)
             VALUES (1, 'Europe');

-- insert into countries
INSERT INTO countries (country_id, country_name, region_id)
               VALUES ('CH', 'Switzerland', 1);

-- insert into locations
INSERT INTO locations (location_id,street_address, postal_code, city, state_province, country_id)
               VALUES (3000, 'Murtenstrasse 921', '3095 ', 'Bern', NULL, 'CH');

-- insert into departments
INSERT INTO departments (department_id, department_name, manager_id, location_id)
                 VALUES (30, 'Purchasing', NULL, 3000);

-- insert into jobs
INSERT INTO jobs (job_id, job_title, min_salary, max_salary)
          VALUES ('PU_MAN', 'Purchasing Manager', 8000, 15000);
INSERT INTO jobs (job_id, job_title, min_salary, max_salary)
          VALUES ('PU_CLERK', 'Purchasing Clerk', 2500, 5500);

-- insert into employees
INSERT INTO employees (employee_id, first_name, last_name, email, phone_number, hire_date, job_id, salary, commission_pct, manager_id, department_id)
               VALUES (100, 'Winston', 'Taylor', 'winston_taylor@gmail.com', '+373662353', '24-JAN-06', 'PU_MAN', 10000, NULL, NULL, 30);

-- insert into job_history
INSERT INTO job_history (employee_id, start_date, end_date, job_id, department_id)
                 VALUES (100, '09-FEB-00', '06-JAN-02', 'PU_CLERK', 30);