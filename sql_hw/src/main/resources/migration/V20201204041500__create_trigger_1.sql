ALTER TABLE locations
ADD department_amount NUMBER DEFAULT 0;

UPDATE locations
SET department_amount = (
                         SELECT COUNT(*)
                         FROM departments d
                         WHERE d.location_id = locations.location_id
                        );

COMMENT ON COLUMN locations.department_amount
IS 'Contains the amount of departments in the location';

CREATE OR REPLACE TRIGGER recount_department_amount_on_insert
    AFTER INSERT OR DELETE
    ON departments
    FOR EACH ROW
BEGIN
    IF INSERTING THEN
        UPDATE locations
        SET  department_amount = department_amount + 1
        WHERE location_id = :NEW.location_id;
    ELSIF DELETING THEN
        UPDATE locations
        SET  department_amount = department_amount - 1
        WHERE location_id = :OLD.location_id;
    END IF;
END recount_department_amount_on_insert;